# -*- coding: utf-8 -*-
"""
Created on Thu Feb 04 13:03:19 2016

@author: mateo
"""

import re
import fileinput
from nltk import PorterStemmer

STOPWORDS = "stopwords.txt"

def main():
    """
    Reads in a song from stdin, removes the stopwords and stems the remaining
    words. Prints output to stdout.
    """
    stemmer = PorterStemmer()
    stopwordfile = fileinput.input(STOPWORDS)
    stopwords = [re.sub(r"[\n\r]", "", word) for word in stopwordfile]
    song = ""

    for line in fileinput.input():  # stdin
        words = re.sub(r"[^a-z ]", "", line.lower()).split()
        # gowords are the opposite of stopwords
        gowords = [w for w in words if w not in stopwords]
        if len(gowords) <= 0:
            continue
        stems = [stemmer.stem(w) for w in gowords]
        song += ':'.join(stems) + ":"
    song = song[:-1]
    print song

main()
