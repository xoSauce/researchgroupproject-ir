##How to run
Type "make" and press return.
 Then type something like "./author_topic input.tsv 3 15 5000 100"

## Author-Topic Model
### usage

    make author_topic
    ./author_topic input.tsv alpha t iter lim
    
### params
- input.tsv: input file(separated \t and :). Each line is document.  
  - format: author:author:...:author \t word:word:word ...  :word
- alpha: \alpha.  
  - \beta if fix(= 0.01)  
- t: # of topics.  
- iter: # of gibbs sampling iterations.  
- lim: limit of outputs.   
