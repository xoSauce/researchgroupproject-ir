# -*- coding: utf-8 -*-
"""
Created on Thu Mar 03 10:15:13 2016

@author: mateo
"""
import sys
import re

for line in sys.stdin:
    nogenre = re.sub(r".*\t", "", line)
    spaces = re.sub(":", " ", nogenre)
    sys.stdout.write(spaces)
