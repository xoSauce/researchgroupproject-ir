# -*- coding: utf-8 -*-
"""
Created on Mon Feb 22 13:03:10 2016

@author: mateo
"""

import psycopg2
import psycopg2.extras

DBKWARGS = {'database': 'musiclyrics_db', 'user': 'awesomeslacker',
            'password': 'many_lyrics', 'port': 5433,
            'host': 'musiclyricsdbinstance.c8bwuwlj9ozr.eu-west-1.rds.amazonaws.com'}
try:
    conn = psycopg2.connect(**DBKWARGS)
except:
    print "Database connection failed with arguments " + str(DBKWARGS)


def exec_query(query):
    conn.reset()
    cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
    cur.execute(query)
    return cur
