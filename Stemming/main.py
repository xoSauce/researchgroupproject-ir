# -*- coding: utf-8 -*-
"""
Created on Tue Feb 23 11:42:59 2016

@author: mateo
"""
import dbmanager
import prep
prepstat = "SELECT s.url as surl, s.lyrics as lyrics, g.name as genre, " \
           "       g.url as gurl " \
           "FROM song s, genre g, artist a, album_to_artist ata, " \
           "     song_to_album sta, album_to_genre atg " \
           "WHERE s.url = sta.song_url AND " \
           "      sta.album_url = atg.album_url AND " \
           "      atg.genre_url = g.url AND " \
           "      sta.album_url = ata.album_url AND " \
           "      ata.artist_url = a.url"


def format_songs(generator):
    for song in generator:
        if song is None:
            continue
        preplyrics = prep.prep_song(song['lyrics'])
        genres = get_supergenres(song['gurl']) + [song['genre']]
        yield (song['surl'], preplyrics, genres)


def get_all_songs():
    cur = dbmanager.exec_query(prepstat + ";")
    for row in cur:
        yield row


def get_some_songs(n):
    execstat = prepstat + " LIMIT {0};".format(n)
    cur = dbmanager.exec_query(execstat)
    for row in cur:
        yield row


def get_supergenres(url):
    prepstatement = "SELECT g.name, g.url " \
                    "FROM genre g, genre_to_subgenre gts " \
                    "WHERE gts.subgenre_url = '{0}' AND gts.genre_url = g.url;"
    execstatement = prepstatement.format(url)
    cur = dbmanager.exec_query(execstatement)
    rows = cur.fetchall()
    supergs = [r[0] for r in rows]
    for r in rows:
        supergs += get_supergenres(r[1])
    return list(set(supergs))
