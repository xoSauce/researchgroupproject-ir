# -*- coding: utf-8 -*-
"""
Created on Thu Feb 25 13:12:11 2016

@author: mateo
"""
import mmap
import re
from sys import stdout


def format_lda(generator, fname):
    with open(fname, "w") as out:
        out.write("       \n")
        total_docs = 0
        for (_, lyrics, _) in generator:
            out.write(lyrics + "\n")
            total_docs += 1
    with open(fname, "r+") as out:
        mm = mmap.mmap(out.fileno(), 8)
        numstr = list(str(total_docs))
        for i in range(len(numstr)):
            mm[i] = numstr[i]


def format_atm(generator, fname):
    with open(fname, "w") as out:
        for (_, lyrics, genre) in generator:
            out.write(":".join(genre) + "\t")
            out.write(re.sub(" +", ":", lyrics) + "\n")


def format_atm_stdout(generator):
    for (_, lyrics, genre) in generator:
        stdout.write(":".join(genre) + "\t")
        stdout.write(re.sub(" +", ":", lyrics) + "\n")


def format_btm(generator, fname):
    with open(fname, "w") as out:
        out.write("       \n")
        for (_, lyrics, _) in generator:
            out.write(lyrics + "\n")
