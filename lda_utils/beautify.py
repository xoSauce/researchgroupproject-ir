from stemming import prep
import time
import csv

if __name__ == '__main__':
	row_count = 0
	output = ""	
	with open('file.csv', 'rt') as csvfile:
		reader = csv.reader(csvfile)
		for row in reader:
			row_count += 1
			output += prep.prep_song(row)
			output += "\n"
	with open('input_file-'+str(time.time()), 'w+') as out:
		out.write("%d\n" % row_count)
		out.write("%s" % output) 
