# -*- coding: utf-8 -*-
"""
Created on Thu Feb 04 13:03:19 2016

@author: mateo
"""

import re
import fileinput
from nltk import PorterStemmer

STOPWORD_FILE = "stopwords.txt"
stemmer = PorterStemmer()
stopwordfile = fileinput.input("stopwords.txt")
stopwords = [re.sub(r"[\n\r]", "", word) for word in stopwordfile]


def prep_song(song):
    """
    Removes stopwords and stems all remaining words, returning the result.
    """
    if song is None:
        return ""
    lines = [prep_line(line) for line in song]
    return ' '.join(lines)


def prep_line(line):
    """
    Removes stopwords from a line, then stems the words and returns the string.
    """
    words = re.sub(r"[^a-z ]+", "", line.lower()).split()
    # gowords are the opposite of stopwords
    gowords = [w for w in words if w not in stopwords]
    stems = [stemmer.stem(w) for w in gowords]
    return ' '.join(stems)
